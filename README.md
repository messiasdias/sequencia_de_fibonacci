# Sequencia de Fibonacci
Formula da Sequência de Fibonacci para array PHP

> F[n] = F[n-1] + F[n-2]

```
$numbers = [0,1];

for($i=2 ; $i <= $max ; $i++ ){
  $numbers[$i] = $numbers[$i-1] + $numbers[$i-2];
}
  
```
